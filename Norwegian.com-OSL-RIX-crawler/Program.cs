﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

namespace Norwegian.com_OSL_RIX_crawler
{
   class Program
   {
      public Dictionary<string, string> paramDic = new Dictionary<string, string>();
      public List<FlightData> flightsDataList = new List<FlightData>();
      public List<FlightData> cheapestFlightsDataList = new List<FlightData>();

      static void Main(string[] args)
      {
         Program p = new Program();

         var task = Task.Run(() => p.DataExtraction());
         task.Wait();

         Thread.Sleep(150000);
         //Console.ReadLine();
      }

      public void SelectCheapest()
      {
         cheapestFlightsDataList = flightsDataList.GroupBy(g => g.DepartureDate).Select(s => s.OrderBy(o => o.Price)).Select(s => s.First()).ToList();
      }

      public void WriteData()
      {
         using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"data.txt"))
         {
            foreach (var item in cheapestFlightsDataList)
            {
               file.WriteLine(item.DepartureDate + " " + item.DepartureAirport + " " + item.ArrivalAirport + " " + item.DepartureTime + " " + item.ArrivalTime + " " + item.Price);
               foreach (var tax in item.FlightTaxes)
               {
                  file.WriteLine("     " + tax.TaxName + " " + tax.TaxValue);
               }
            }
         }
      }

      public async void DataExtraction()
      {
         int year = 0;
         int month = 0;
         var splitByQuestionMark = SearchParameters(out year, out month);
         var days = DateTime.DaysInMonth(year, month);

         try
         {
            for (int day = 1; day <= days; day++)
            {
               var updatedUrl = UpdateUrl(day, splitByQuestionMark);
               await StartCrawlerAsync(updatedUrl);
               Console.WriteLine(day + " atlikta");
               if (day == days)
               {
                  SelectCheapest();
                  WriteData();
                  Console.WriteLine("Įrašiau duomenis.");
               }
            }
         }
         catch (Exception ex)
         {
            throw ex;
         }
         

      }


      public async Task StartCrawlerAsync(string url)
      {

         var httpClient = new HttpClient();
         var html = await httpClient.GetStringAsync(url);

         var htmlDocument = new HtmlDocument();
         htmlDocument.LoadHtml(html);

         var isFlyingList = htmlDocument.DocumentNode.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("sectionboxavaday avadayoutbound")).ToList();
         var isFlying = isFlyingList.Count != 0 ? true : false;



         if (isFlying)
         {
            var eventTarget = htmlDocument.DocumentNode.SelectSingleNode("//a[@class='HiddenControl postbackfaker']").Attributes["id"].Value;
            var eventArguments = htmlDocument.DocumentNode.SelectNodes("//input[@type='radio']").Where(node => node.GetAttributeValue("id", "").Contains("FlightSelectOutbound")).Select(node => node.Attributes["value"].Value).ToList();
            var viewState = htmlDocument.DocumentNode.SelectSingleNode("//input[@id='__VIEWSTATE']").Attributes["value"].Value;

            var departureAirports = htmlDocument.DocumentNode.SelectNodes("//tr[contains((@class), 'rowinfo2')]//td[@class='depdest']//div[@class='content']").Select(node => node.InnerText).ToList();
            var arrivalAirports = htmlDocument.DocumentNode.SelectNodes("//tr[contains((@class), 'rowinfo2')]//td[@class='arrdest']//div[@class='content']").Select(node => node.InnerText).ToList();
            var connectionAirports = htmlDocument.DocumentNode.SelectNodes("//tr[contains((@class), 'lastrow')]//td[@class='routeinfotextcell routeinfoimagecell']//div//ul//li").Select(node => node.InnerText).ToList();
            var prices = htmlDocument.DocumentNode.SelectNodes("//tr//td[starts-with(@class,'fareselect')]//div//label").Select(node => node.InnerText).ToList();
            var departureTimes = htmlDocument.DocumentNode.SelectNodes("//tr//td[@class='depdest']//div[@class='content emphasize']").Select(node => node.InnerText).ToList();
            var arrivalTimes = htmlDocument.DocumentNode.SelectNodes("//tr//td[@class='arrdest']//div[@class='content emphasize']").Select(node => node.InnerText).ToList();
            var isDirect = htmlDocument.DocumentNode.SelectNodes("//tr[contains((@class), 'rowinfo1')]//td[@class='duration']//div[@class='content']").Select(node => node.InnerText).ToList();

            for (int i = 0; i < eventArguments.Count; i++)
            {
               var j = i / 3;
               if (isDirect[j] == "Direct")
               {
                  var content = new FormUrlEncodedContent(new[]
                  {
                     new KeyValuePair<string, string>("__EVENTTARGET", eventTarget), // "ctl00_MainContent_ipcAvaDay_ipcResultOutbound_lbtPostBackFaker"
                     new KeyValuePair<string, string>("__EVENTARGUMENT", eventArguments[i]), // "0|DY1072OSLRIX|1|0|0"
                     new KeyValuePair<string, string>("__VIEWSTATE", viewState), // daug info
                  });

                  var result = httpClient.PostAsync(url, content).Result;
                  var res = result.Content.ReadAsStringAsync().Result;

                  var htmlDocTaxes = new HtmlDocument();
                  htmlDocTaxes.LoadHtml(res);

                  var taxesNames = htmlDocTaxes.DocumentNode.SelectNodes("//tr//td[@class='leftcell' and @valign='top' and contains(text(), 'surcharge')]").Select(node => node.InnerText).ToList();
                  var taxesValues = htmlDocTaxes.DocumentNode.SelectNodes("//tr//td[@class='rightcell' and @align='right' and contains(text(), '€')]").Select(node => node.InnerText).ToList();

                  var flightTaxes = new List<Tax>();

                  for (int n = 0; n < taxesNames.Count; n++)
                  {
                     var tax = new Tax(
                        taxesNames[n],
                        float.Parse(Regex.Replace(taxesValues[n], "[^0-9.]", ""), CultureInfo.InvariantCulture)
                        );
                     flightTaxes.Add(tax);
                  }

                  var flightData = new FlightData(
                     departureAirports[j],
                     arrivalAirports[j],
                     paramDic["D_Month"] + paramDic["D_Day"],
                     departureTimes[j],
                     arrivalTimes[j],
                     float.Parse(prices[i], CultureInfo.InvariantCulture),
                     flightTaxes
                     );

                  flightsDataList.Add(flightData);
               }
            }

         }

      }

      private string UpdateUrl(int day, List<string> splitByQuestionMark)
      {
         var dayStr = day.ToString("00");
         paramDic["D_Day"] = dayStr;

         string updatedParamStr = "";

         for (int pair = 0; pair < paramDic.Count; pair++)
         {
            updatedParamStr += paramDic.Keys.ElementAt(pair) + "=" + paramDic.Values.ElementAt(pair);
            if (pair != paramDic.Count - 1)
               updatedParamStr += "&";
         }

         var updatedUrl = splitByQuestionMark[0] + "?" + updatedParamStr;

         return updatedUrl;
      }

      public List<string> SearchParameters(out int year, out int month)
      {
         var url = "https://www.norwegian.com/en/ipc/availability/avaday?A_City=RIX&AdultCount=1&ChildCount=0&CurrencyCode=EUR&D_City=OSL&D_Day=05&D_Month=201809&D_SelectedDay=01&IncludeTransit=true&InfantCount=0&R_Day=11&R_Month=201808&TripType=1&mode=ab";

         var splitByQuestionMark = url.Split('?').ToList();
         var searchParameters = splitByQuestionMark[1].Split('&').ToList();

         foreach (var param in searchParameters)
         {
            var key = param.Substring(0, param.LastIndexOf('='));
            var value = param.Substring(param.LastIndexOf('=') + 1);
            paramDic.Add(key, value);
         }

         string yearMonth;
         paramDic.TryGetValue("D_Month", out yearMonth);
         year = int.Parse(yearMonth.Substring(0, 4));
         month = int.Parse(yearMonth.Substring(4));

         return splitByQuestionMark;
      }
   }
}
