﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Norwegian.com_OSL_RIX_crawler
{
   public class Tax
   {
      public string TaxName { get; set; }
      public float TaxValue { get; set; }

      public Tax(string taxName, float taxValue)
      {
         TaxName = taxName;
         TaxValue = taxValue;
      }
   }
}
