﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Norwegian.com_OSL_RIX_crawler
{
   public class FlightData
   {
      public string DepartureAirport { get; set; }
      public string ArrivalAirport { get; set; }
      public string DepartureDate { get; set; }
      public string DepartureTime { get; set; }
      public string ArrivalTime { get; set; }
      public float Price { get; set; }
      public List<Tax> FlightTaxes = new List<Tax>();
      //public List<Tax> Taxes = new List<Tax>();

      public FlightData(string departureAirport, string arrivalAirport, string departureDate, string departureTime, string arrivalTime, float price, List<Tax> flightTaxes)
      {
         DepartureAirport = departureAirport;
         ArrivalAirport = arrivalAirport;
         DepartureDate = departureDate;
         DepartureTime = departureTime;
         ArrivalTime = arrivalTime;
         Price = price;
         FlightTaxes = flightTaxes;
      }

      //public void AddTax(string taxName, string taxValue)
      //{
      //   Taxes.
      //}

   }
}
